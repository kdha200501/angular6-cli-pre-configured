##### 1. generate component for a module

```shellscript
$ npm run ng generate component ../<module-name>/<component-name> -- --prefix=app-<module-name> --module=../<module-name>/app
```
> the `--module=<module-name>` flag specifies in which module the new component is to be declared

##### 2. create interface for a new model

```shellscript
$ npm run ng generate interface ../<module-name>/<component-name>/<component-name> -- --type=interface --prefix=I
```

for example:

```javascript
/*
  src/someModule/foo-bar/foo-bar.interface.ts
*/
export interface IFooBar {
  baz: string;
}
```

##### 3. generate the model

```shellscript
$ npm run ng generate class ../<module-name>/<component-name>/<component-name> -- --type=model
```

for example:

```javascript
/*
  src/someModule/foo-bar/foo-bar.model.ts
*/
export class FooBar implements IFooBar{
  constructor() {
  }

  // Private Variables
  private _baz: string;

  // Accessors
  public get baz(): string {
    return this._baz;
  }

  public set baz(value: string) {
    this._baz = value;
  }
}
```

##### 4. add model to the new component

for example:

```javascript
/*
  src/someModule/foo-bar/foo-bar.component.ts
*/

// Private Variables
private _fooBar: FooBar;

// Accessors
public get fooBar(): FooBar {
  return this._fooBar;
}
```

##### 5. create pipe for the new model [optional]

```shellscript
$ npm run ng generate pipe ../<module-name>/<component-name> --  --flat=false --module=../<module-name>/app
```