##### 1. inject app store into a component

```javascript
/*
  src/someModule/foo-bar/foo-bar.component.ts
*/

constructor(private _store: Store<IAppStore>) {
}
```

##### 2. subscribe to state changes [with the `async` pipe]

this approach works with primitive *json* objects, and it is less code

```html
<!--
  src/someModule/foo-bar/foo-bar.component.html
-->

<ng-container *ngIf="(fooBar$ | async) as fooBar">
  <p>
    baz: {{ fooBar.baz }}
  </p>
</ng-container>
```

> the `async` pipe is an *impure* pipe that takes care of the observable's lifecycle

> `ng-container` transcludes, it does not render into *DOM*

```javascript
/*
  src/someModule/foo-bar/foo-bar.component.ts
*/

public fooBar$: Observable<IFooBar>; // the trailing '$' means observable

ngOnInit() {
  this.fooBar$ = this._store.pipe(selectFooBar);
}
```

##### 3. subscribe to state changes [alternative]

when working with a model (as opposed to primitive *json* object), the observable's lifecycle needs to be accounted for

```html
<!--
  src/someModule/foo-bar/foo-bar.component.html
-->

<p>
  baz: {{ fooBar?.baz }}
</p>
```

```javascript
/*
  src/someModule/foo-bar/foo-bar.component.ts
*/

constructor(private _store: Store<IAppStore>) {
  this._fooBar = new FooBar();
}

// Private Variables
private _stateSubscription: Subscription;
private _fooBar: FooBar;

ngOnInit() {
  this._stateSubscription = this._store
    .pipe(
      selectFooBar,
      map((fooBarObj: IFooBar) => {
        this._fooBar.baz = fooBarObj.baz;
      })
    )
    .subscribe();
}

ngOnDestroy() {
  this._stateSubscription.unsubscribe();
}
```
