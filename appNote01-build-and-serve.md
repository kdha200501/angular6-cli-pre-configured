##### 1. dev build

```shellscript
$ npm start -- --open
```

> launches the default browser and navigates to localhost:4200

##### 2. prod build

```shellscript
$ npm run build -- --configuration=production

$ cd dist/<project-name>

$ python -m SimpleHTTPServer 8081
```

> use an open port like `8080` to serve in local area network

the `--configuration=production` flag refers to the following block of settings in `angular.json`:

```javascript
"production": {
  "fileReplacements": [
    {
      "replace": "src/environments/environment.ts",
      "with": "src/environments/environment.prod.ts"
    }
  ],
  "optimization": true,
  "outputHashing": "all",
  "sourceMap": false,
  "extractCss": true,
  "namedChunks": false,
  "aot": true,
  "extractLicenses": true,
  "vendorChunk": false,
  "buildOptimizer": true
}
```
