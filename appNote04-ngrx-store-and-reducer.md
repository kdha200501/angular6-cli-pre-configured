##### 1. generate a ngrx module for the root module

```shellscript
$ npm run ng generate module app-ngrx -- --spec=false --module=app --flat
```

then assign `StoreModule.forRoot` to the ngrx module:

```javascript
/*
  src/app/app-ngrx.module.ts
*/
imports: [
  StoreModule.forRoot({})
]
```

##### 2. generate a ngrx module for a lazyloaded module

```shellscript
$ npm run ng generate module ../<module-name>/app-ngrx -- --spec=false --module=../<module-name>/app --flat
```
> the `--module=../<module-name>` flag specifies the ngrx module is to be imported by the lazyloaded module

then assign `StoreModule.forFeature` to the ngrx module

```javascript
/*
  src/someModule/app-ngrx.module.ts
*/
imports: [
  StoreModule.forFeature(AppEnum.STORE_NAME, REDUCER_TOKEN)
]

providers: [{
  provide: REDUCER_TOKEN,
  useFactory: getActionReducerMap
}]
```

> `REDUCER_TOKEN` refers to an `ActionReducerMap` which is provided through a getter

> the tokenization approach is used to avoid defining `ActionReducerMap` statically when `ActionReducerMap` contains variables, these variables cannot be analyzed statically and will cause *AOT* compilation to fail see [*ref*](https://github.com/ngrx/platform/blob/master/docs/store/api.md#injecting-reducers)

##### 3. create enum for the lazyloaded module

```shellscript
$ npm run ng generate enum ../<module-name>/app
```
then add enum:

```javascript
export enum AppEnum {
  STORE_NAME = '<module-name>'
}
```

##### 4. describe the store's data structure

```shellscript
$ npm run ng generate interface ../<module-name>/app-store -- --type=interface --prefix=I
```

> a store is the equivalent of a *state machine*, its **reducer** derives state change from actions received

then, describe the data structure for state, store and action using interfaces

```javascript
/*
  src/someModule/app-store.interface.ts
*/

// Data structure for the store
export interface IAppStore {
  [AppEnum.STORE_NAME]: IState;
}

export interface IState extends IFooBarState, IAnotherModelState {
}

// Data structure for states
export interface IFooBarState {
  fooBar: IFooBar;
}

export interface IAnotherModelState {
  anotherModelState: ...
}

// Data structure for actions
export interface IAction extends Action {
  payload?: IFooBarState | IAnotherModelState;
}

// utilities for subscribers
const selectState = pipe( select(AppEnum.STORE_NAME), map(obj => obj[AppEnum.STORE_NAME]) );

export const selectFooBar = pipe(selectState, map((state: IFooBarState) => state.fooBar));

export const selectAnotherModel = pipe(selectState, map((state: IAnotherModelState) => ...));
```

then, add reducer:

> actions being handled in the *reducer* should be strictly `CRUD` *i.e.* the *reducer* should derive state change in one of `create`, `read`, `update`, and `delete`

> more complex actions should be handled in the *effects* module where `CRUD` actions are spawned, see later

##### 5. create the reducer

```shellscript
$ npm run ng generate class ../<module-name>/app-store -- --type=reducer
```

then, replace with:

```javascript
/*
  src/someModule/app-store.reducer.ts
*/

const initState: IState = {
  fooBar: {
    baz: 'Hello World!'
  },
  anotherModelState: ...
};

// use an angular token for the reducer map instead of static implementation
export const REDUCER_TOKEN = new InjectionToken<ActionReducerMap<IAppStore>>(`${AppEnum.STORE_NAME}-reducer-map`);

// provide for the angular token
export function getActionReducerMap(): ActionReducerMap<IAppStore> {
  return {
    // the token + provider approach spares the reducer map's key (which is a variable) to be statically analyzed
    [AppEnum.STORE_NAME]: function (state: IState = initState, action: IAction): IState {
      switch (action.type) {
        default:
          return state;
      }
    }
  };
}
```
> the `ActionReducerMap<IAppStore>` typing binds the reducer function to `IAppStore`, static analysis implies that, the reducer function receives and returns state object of `IState` interface
