##### 1. generate module with routing

```shellscript
$ npm run ng generate module ../<module-name>/app -- --routing --flat --spec=false
```
> the `../<module-name>` path is relative to the `app` folder, it creates the new module folder at the same level as `app`

##### 2. generate the root component for the new module

```shellscript
$ npm run ng generate component ../<module-name>/app -- --flat --selector=app-<module-name> --module=../<module-name>/app
```
> the `--module=../<module-name>` flag specifies the new component is to be declared in the new module

then, assign the new module's root component to its root path

```javascript

/*
  src/<module-name>/app-routing.module.ts
*/

const routes: Routes = [{
  path: '',
  component: AppComponent
}];
```

##### 3. lazy-load module

- assign the new module to a child path of the root app

```javascript

/*
  src/app/app-routing.module.ts
*/

const routes: Routes = [{
  path: 'url-path-for-module',
  loadChildren: '../<module-name>/app.module#AppModule'
}];
```

- the child path to the new module can be referenced as:

```html
<a routerLink="/url-path-for-module">lazy-load module</a>
```

##### 4. always load module [alternative]

- export the new module's root component

```javascript

/*
  src/<module-name>/app-routing.module.ts
*/

@NgModule({
  exports: [AppComponent]
})
```

- assign the new module's root component to a child path of the root app

```javascript

/*
  src/app/app-routing.module.ts
*/

import { AppComponent as SomeRootComponent } from '../<module-name>/app.component';

const routes: Routes = [{
  path: 'url-path-for-module',
  component: SomeRootComponent
}];
```

- import the new module and declare its root component

```javascript

/*
  src/app/app.module.ts
*/

import { AppModule as SomeModule } from '../<module-name>/app.module';

@NgModule({
  imports: [
    SomeModule
  ]
})
```