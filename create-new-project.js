#!/usr/bin/env node

/*
global require, module
*/

const { dirname, basename, join } = require('path');
const { mkdirSync } = require('fs');
const { spawn } = require('child_process');

const $0 = process.argv[1];
const $1 = process.argv[2];

const pathAngularCliRelative = 'node_modules/@angular/cli/bin/ng';
const pathAngularCli = join( dirname($0), pathAngularCliRelative );

// 1. validate project path
if( (typeof $1).toLowerCase() !== 'string' || $1.trim() === '' ) {
	console.error('Error: must provide project path');
	process.exit(1);
}

// 2. create project path
mkdirSync($1, {recursive: true});

// 3. scaffold project
let angularCliPreset = [
	'new',
	'--style', 'scss',
	'--skip-install', 'true',
	'--routing', 'true',
//	'--skip-git', 'true',
	basename($1)
];
let createNewApp = spawn(pathAngularCli, angularCliPreset, {cwd: dirname($1)});

// 4. send log to console
createNewApp.stdout.on('data', (data) => {
	console.log(`stdout: ${data}`);
});
createNewApp.stderr.on('data', (data) => {
	console.log(`stderr: ${data}`);
});
createNewApp.on('close', (code) => {
	console.log(`child process exited with code ${code}`);
});
