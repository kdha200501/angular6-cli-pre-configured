##### 1. generate a library workspace within the project

```shellscript
$ npm run ng generate library <library-name>
```

##### 2. expose injectable service

```javascript
/*
  projects/some-library/src/lib/some-library.module.ts
*/

@NgModule({
  providers: [someService]
})
```

> other const/class/interface/enum can be exposed through `projects/<library-name>/src/public_api.ts`

##### 3. unit test the library

```shellscript
$ npm test <library-name> -- --code-coverage
```

> the code coverage report is generated at `/coverage`

##### 4. buid the library

```shellscript
$ npm run ng build <library-name>
```

##### 5. publish the library

```shellscript
$ cd dist/<library-name>

$ npm publish
```

> the library gets published, not the project
