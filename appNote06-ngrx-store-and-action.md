##### 1. add action type to enum

```javascript
/*
  src/someModule/app.enum.ts
*/

export enum AppEnum {
  FOO_BAR_ACTION_UPDATE = 'FOO_BAR_ACTION_UPDATE'
}
```

##### 2. add action to reducer

```javascript
/*
  src/someModule/app-store.reducer.ts
*/

switch (action.type) {
  case AppEnum.FOO_BAR_ACTION_UPDATE:
    state.fooBar.baz = (<IFooBarState>action.payload).fooBar.baz;
    return {...state};
  default:
    return state;
}
```

##### 3. generate a directive

```shellscript
$ npm run ng generate directive ../<module-name>/<directive-name>/<directive-name> -- --prefix=app-<module-name> --module=../<module-name>/app
```

then, inject the app store:

```javascript
constructor(private _store: Store<IAppStore>) {
}
```

then, bind host *DOM*'s click event to the new action:

```javascript
@HostListener('click', ['event'])
public onClick(): void {
  this._store.dispatch(<IAction>{
    type: AppEnum.FOO_BAR_ACTION_UPDATE,
    payload: {fooBar: {baz: 'updated value'}}
  });
}
```

##### 4. expose the directive

```html
<!--
  src/someModule/app.component.html
-->

<foo-bar></foo-bar>
<!--
  initial value: "Hello World!"
  value after click: "updated value"
-->

<button appSomeDirective>click to update</button>
```
