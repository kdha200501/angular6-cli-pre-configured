##### 1. update ngrx for the root module

add `EffectsModule.forRoot` to the ngrx module:

```javascript
/*
  src/app/app-ngrx.module.ts
*/

imports: [
  EffectsModule.forRoot([])
]
```

##### 2. update ngrx for the lazyloaded module

add `EffectsModule.forFeature` to the ngrx module:

```javascript
/*
  src/someModule/app-ngrx.module.ts
*/

imports: [
  EffectsModule.forFeature([AppEffectsService])
]
```

##### 3. add action type to enum

```javascript
/*
  src/someModule/app.enum.ts
*/

export enum AppEnum {
  SOME_ARBITRARY_ACTION = 'SOME_ARBITRARY_ACTION'
}
```

##### 4. create effects service

```shellscript
$ npm run ng generate service ../<module-name>/app-effects -- --type=service --spec=false
```

then, declare logic that changes *state*

```javascript
const updateFooBar = map(([action, fooBarObjCurrent]) => {
  let payload = (<IAction>action).payload;
  let fooBarObjNext: IFooBar = (<IFooBarState>payload).fooBar;
  fooBarObjNext = someLogic(fooBarObjCurrent, fooBarObjNext);
  return <IAction>{
    type: AppEnum.FOO_BAR_ACTION_UPDATE,
    payload: {
      fooBar: fooBarObjNext
    }
  };
});
```

> the logic takes an arbitrary action and the current state of a model to produce a new `CRUD` action and the next state of the model

then, inject *store* and *action* observable to the new service

```javascript
constructor(private _store: Store<IAppStore>, private _action$: Actions) {
}
```

then, add effects:

```javascript
@Effect()
  SOME_ARBITRARY_ACTION$: Observable<IAction> = this._action$.pipe(
    ofType(AppEnum.SOME_ARBITRARY_ACTION),
    withLatestFrom( this._store.pipe(selectFooBar) ),
    updateFooBar
  );
```

> an *effect* contains logic that maps an arbitrary action to action(s) in the *reducer*

> `ofType(AppEnum.SOME_ARBITRARY_ACTION)` observes this arbitrary action

> `withLatestFrom( this._store.pipe(selectFooBar) )` merges the action observable with the observable of a model's the current state

> `updateFooBar` performs logic that maps to an action in the *reducer*

##### 5. refactor the directive
```javascript
/*
  src/someModule/some-directive/some-directive.directive.ts
*/

@HostListener('click', ['event'])
  public onClick(): void {
    this._store.dispatch(<IAction>{
      type: AppEnum.SOME_ARBITRARY_ACTION,
      payload: {fooBar: {baz: 'new value'}}
    });
  }
```

> instead of dispatching an action in the *reducer* directly, the action is dispatched to `effects` to leverage logic
