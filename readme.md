# Intro.

- use this repository instead of installing the Angular cli globally
- at the time of this writing, Angular is at version `6.1.0`
- scaffold an Angular 6 CLI project with pre-configured settings:

```
--style: scss
--skip-install: true
--routing: true
```

## 1. Installation

```shellscript
$ git clone https://kdha200501@bitbucket.org/kdha200501/angular6-cli-pre-configured.git
$ cd angular6-cli-pre-configured
$ npm install
```

## 2. Usage

```shellscript
$ npm run create-new-project /path/to/new/project
```
> this will create the project directory recursively

> a project hosts workspaces for an app and/or libraries

> a reusable library should be hosted in its exclusive project

```shellscript
$ cd /path/to/new/project

$ npm install
```

## 3. Guidelines for building an angular project

A project is easy to maintain when the file structure follows a consistent pattern, and separation of concern is insisted

### 3.1 Building an Angular app

The essence is to put styling in `.scss`, describe *view-model* in a `.component.ts`, implement logic in `.service.ts`, manage state with *ngrx*

Install *ngrx*:

```shellscript
$ npm install @ngrx/effects@^6.1.1

$ npm install @ngrx/store@^6.1.1
```

- [build-and-serve](appNote01-build-and-serve.md)
- [lazyload-modue](appNote02-lazyload-modue.md)
- [component-and-model](appNote03-component-and-model.md)
- [ngrx-store-and-reducer](appNote04-ngrx-store-and-reducer.md)
- [ngrx-store-and-state-observable](appNote05-ngrx-store-and-state-observable.md)
- [ngrx-store-and-action](appNote06-ngrx-store-and-action.md)
- [ngrx-effects-and-action](appNote07-ngrx-effects-and-action.md)

### 3.2 Building an Angular library

- [build](libNote01-build.md)
